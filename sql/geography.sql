BEGIN TRANSACTION;

INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (1, 1, 'Dalelands');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (2, 1, 'Cormyr');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (3, 1, 'Sembia');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (4, 1, 'The Moon Sea');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (5, 1, 'The Vast');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (6, 1, 'The Dragon Coast');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (7, 1, 'Western Heartlands');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (8, 1, 'Island Kingdoms');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (9, 1, 'Savage North');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (10, 1, 'Anauroch');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (11, 1, 'The Cold Lands');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (12, 1, 'The Unapproachable East');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (13, 1, 'The Old Empires');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (14, 1, 'Vilhon Reach');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (15, 1, 'Empire of the Sands');
INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (16, 1, 'The Shining South');
--INSERT INTO GEO_AREA ([ID], [CAMPAIGN_ID], [NAME]) VALUES (17, 1, 'The Lost Kingdoms');

COMMIT TRANSACTION;

-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION;

-- Dalelands
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (1, 1, 'Archendale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (2, 1, 'Battledale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (3, 1, 'Daggerdale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (4, 1, 'Deepingdale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (5, 1, 'Harrowdale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (6, 1, 'High Dale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (7, 1, 'Mistledale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (8, 1, 'Moondale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (9, 1, 'Scardale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (10, 1, 'Sessrendale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (11, 1, 'Shadowdale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (12, 1, 'Tarkhaldale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (13, 1, 'Tasseldale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (14, 1, 'Teshendale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (15, 1, 'Elven Woods');

-- Cormyr
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (16, 2, 'Cormyr Kingdom');

-- Sembia
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (17, 3, 'Nation of Sembia');

-- The Moon Sea
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (18, 4, 'Moonsea Area');

-- The Vast
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (19, 5, 'The Vast Area');

-- The Dragon Coast
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (20, 6, 'Dragon Coast Area');

-- Western Heartlands
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (21, 7, 'Western Land');

-- Island Kingdoms
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (22, 8, 'Evermeet');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (23, 8, 'Lantan');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (24, 8, 'Mintarn');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (25, 8, 'The Moonshaes');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (26, 8, 'The Nelanther');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (27, 8, 'Nimbral');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (28, 8, 'Orlumbor');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (29, 8, 'Ruathym');

-- Savage North
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (30, 9, 'Northern Areas');

-- Anauroch
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (31, 10, 'The Great Desert');

INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (32, 11, 'Damara');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (33, 11, 'Glister');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (34, 11, 'The Great Glacier');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (35, 11, 'Narfell');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (36, 11, 'The Ride');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (37, 11, 'Sossal');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (38, 11, 'Thar');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (39, 11, 'Vaasa');

-- The Unapproachable East
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (40, 12, 'Aglarond');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (41, 12, 'Impiltur');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (42, 12, 'The Great Dale');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (43, 12, 'Rashemen');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (44, 12, 'Thay');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (45, 12, 'Thesk');

-- The Cold Lands

-- The Old Empires
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (46, 13, 'The Alamber Sea');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (47, 13, 'Chessenta');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (48, 13, 'Mulhorand');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (49, 13, 'Unther');

-- Vilhon Reach
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (50, 14, 'Chondalwood');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (51, 14, 'Chondath');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (52, 14, 'Gulthmere');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (53, 14, 'Hlondeth');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (54, 14, 'Nimpeth');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (55, 14, 'Sespech');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (56, 14, 'The Shining Plains');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (57, 14, 'Turmish');

-- Empire of the Sands
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (58, 15, 'Amn');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (59, 15, 'Calimshan');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (60, 15, 'Tethyr');

-- The Shining South
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (61, 16, 'The Great Rift');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (62, 16, 'Halruaa');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (63, 16, 'Jungles of Chult');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (64, 16, 'Lake of Steam');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (65, 16, 'Luiren');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (66, 16, 'Raurin');
INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (67, 16, 'The Shaar');

-- The Lost Kingdoms
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (68, 17, 'Anauria');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (69, 17, 'Askavar');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (70, 17, 'Asram');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (71, 17, 'Cormanthyr');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (72, 17, 'Delzoun');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (73, 17, 'Eaerlann');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (74, 17, 'Hlondath');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (75, 17, 'Illefarn');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (76, 17, 'Imaskar');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (77, 17, 'Kingdom of Man');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (78, 17, 'Mulhorand');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (79, 17, 'Narfell');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (80, 17, 'Netheril');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (81, 17, 'Oghrann');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (82, 17, 'Raumathar');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (83, 17, 'Shandaular');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (84, 17, 'Shoon');
-- INSERT INTO REGION ([ID], [GEO_AREA_ID], [NAME]) VALUES (85, 17, 'Unther');

COMMIT TRANSACTION;

-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION;

INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (1, 'City');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (2, 'Road');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (3, 'Ruin');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (4, 'Sacred place');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (5, 'Community');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (6, 'Wood');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (7, 'Mountain');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (8, 'Mountain pass');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (9, 'Stronghold');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (10, 'Village');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (11, 'Swamp');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (12, 'Wasteland');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (13, 'Hilly area');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (14, 'Marsh');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (15, 'Battlefield');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (16, 'Fallen city');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (17, 'Glacier');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (18, 'Ford');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (19, 'Outlands');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (20, 'Bridge');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (21, 'River');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (22, 'Mining centre');
INSERT INTO PLACE_OF_INTEREST_TYPE ([ID], [DESCRIPTION]) VALUES (23, 'Fallen kingdom');

COMMIT TRANSACTION;

-----------------------------------------------------------------------------------------------------------

BEGIN TRANSACTION;

-- Elven Woods
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 4, 'The Elven Court');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 2, 'Halfaxe Trail');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 2, 'Moander''s Road');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 3, 'Myth Drannor');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 5, 'Semberholme');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 4, 'The Standing Stone');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 5, 'The Tangled Trees');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 4, 'Vale of Lost Voices');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 6, 'Border Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 7, 'Desertsmouth Mountains');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 8, 'Shadow Gap');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (15, 6, 'Spiderhaunt Woods');

-- Cormyr Kingdom
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Arabel');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 2, 'Calantar''s Way');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 9, 'Castle Crag');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Dhedluk');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 10, 'Espar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Eveningstar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 11, 'The Farsea Marshes');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 11, 'The Marsh of Tun');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 12, 'Goblin Marches');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 12, 'The High Moors');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 13, 'Helmlands');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 6, 'Hermit''s Wood');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 9, 'High Horn');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 10, 'Hilp');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 6, 'Hullack Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 10, 'Hultail');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Immersea');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 6, 'King''s Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Marsember');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 12, 'Stonelands');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 7, 'Storm Horns');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Suzail');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 7, 'The Thunder Peaks');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Tilverton');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 8, 'Tilver''s Gap');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 10, 'Tyrluk');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 14, 'The Vast Swamp');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Waymoot');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (16, 1, 'Wheloon');

-- Nation of Sembia
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Daerlun');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Ordulin');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Saerloon');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Selgaunt');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Urmlaspyr');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (17, 1, 'Yhaunn');

-- Moonsea Area
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 15, 'The Bell in the Depths');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 9, 'Citadel of the Raven');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 10, 'Elmwood');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 6, 'Elventree');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Hillsfar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 16, 'Hulburg');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 16, 'Sulasspryn');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 9, 'Ironfang Keep');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 5, 'Melvaunt');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Mulmaster');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 16, 'Phlan');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Teshwave');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Thentia');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Voonlar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 16, 'Yulash');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (18, 1, 'Zhentil Keep');

-- The Vast Area
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Calaunt');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 10, 'Dragon Falls');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 11, 'Theh Flooded Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 17, 'Glacierhearts of the White Worm');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 10, 'High Haspur');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Hlintar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'King''s Reach');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Kurth');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 10, 'Maskyr''s Eye');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 10, 'Procampur');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Ravens Bluff');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Sevenecho');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Tantras');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 1, 'Tsurlagol');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 18, 'Viperstongue Ford');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (19, 10, 'Ylraphon');

-- Dragon Coast Area
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Elversult');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Ilipur');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Pros');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 19, 'Pirate Isles of the Inner Sea');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Priapurl');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Proskur');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Reddansyr');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Teziir');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (20, 1, 'Westgate');

-- Western Land
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Asbravn');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Baldur''s Gate');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 15, 'The Battle of Bones');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Beregost');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 20, 'Boareskyr Bridge');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 9, 'Candlekeep');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'Cloak Wood');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 10, 'Corm Orp');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 5, 'Daggerford');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 9, 'Darkhold');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 9, 'Dragonspear Castle');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 3, 'Durlag''s Tower');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 10, 'Easting');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Elturel');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 5, 'Evereska');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 13, 'The Far Hills');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 15, 'Fields of the Dead');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'The Forest of Wyrms');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'The Forgotten Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 9, 'The Friendly Arm');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 13, 'Greycloak Hills');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 9, 'Hammer Hall');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 12, 'The High Moor');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 13, 'Hill of Lost Souls');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 5, 'Hill''s Edge');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Hluthvar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 23, 'Illefarn');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Iriaebor');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 12, 'The Laughing Hollow');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 14, 'Lizard Marsh');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 12, 'The Lonely Moor');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 14, 'Marsh of Chelimber');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'Misty Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'The Reaching Woods');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Scornubel');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 13, 'Serpent Hills');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 19, 'Skull Gorge');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Soubar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 7, 'Sunset Mountains');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 19, 'Sword Coast');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Triel');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 5, 'Trielta HilLs');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 18, 'Trollclaw Ford');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 13, 'Troll Hills');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'Trollbark Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 10, 'The Way Inn');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 21, 'Winding Water');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 6, 'The Wood of Sharp Teeth');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 1, 'Waterdeep');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (21, 8, 'Yellow Snake Pass');

-- Northern Areas
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 19, 'Barbarian Peoples');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 5, 'Citadel Adbar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 9, 'Hellgate Keep');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 6, 'High Forest');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 5, 'Llorkh');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 1, 'Luskan');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 22, 'Mirabar');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 1, 'Neverwinter');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 1, 'Silverymoon');
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (30, 9, 'Sundabar');

-- Thesk
INSERT INTO PLACE_OF_INTEREST ([REGION_ID], [TYPE], [NAME]) VALUES (45, 1, 'Telflamm');

COMMIT TRANSACTION;

-----------------------------------------------------------------------------------------------------------

